package com.curso.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.curso.database.entities.Curso;

public interface CursoRepository extends JpaRepository<Curso, Integer>{
	
	List<Curso> findCursoByNome(String nome);
	
	List<Curso> findCursoByNomeContaining(String valor);
	
	List<Curso> findCursoByNomeLike(String valor);
	
	List<Curso> findCursoByNomeLikeIgnoreCase(String valor);
	
	@Query(value = "select c from Curso c")
	List<Curso> findByQueryNome();
	
	@Query(value = "select nome_do_curso from Curso where area = 'Exatas' order by id", nativeQuery = true)
	List<String> findByQueryNomePorArea();
	
	@Query(value = "select nome_do_curso from Curso where area = :area order by id", nativeQuery = true)
	List<String> findByQueryNomePorAreaInformada(@Param("area") String area);
	
	@Query(value = "select nome_do_curso from Curso where area = :area and nome_do_curso = :nome ", nativeQuery = true)
	List<String> findByQueryNomePorAreaCursoInfo(@Param("area") String area,@Param("nome") String nome);
	
	@Query(value = "select nome_do_curso from Curso where area = ?1 and nome_do_curso = ?2", nativeQuery = true)
	List<String> findByQueryNomePorAreaCursoInfoParam(String area, String nome);
}

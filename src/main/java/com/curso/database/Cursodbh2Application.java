package com.curso.database;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import com.curso.database.entities.Aluno;
import com.curso.database.entities.Curso;
import com.curso.database.entities.GradeCurricular;
import com.curso.database.entities.Materia;
import com.curso.database.repositories.AlunoRepository;
import com.curso.database.repositories.CursoRepository;
import com.curso.database.repositories.GradeCurricularRepository;
import com.curso.database.repositories.MateriaRepository;

@SpringBootApplication
public class Cursodbh2Application implements CommandLineRunner{
	
	@Autowired
	private CursoRepository cursoRepository;
	
	@Autowired
	private AlunoRepository alunoRepository;
	
	@Autowired
	private GradeCurricularRepository gradeRepository;
	
	@Autowired
	private MateriaRepository materiaRepository;

	public static void main(String[] args) {
		SpringApplication.run(Cursodbh2Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//criar cursos ofertados
		Curso curso1 = new Curso("Graduação em TI", "Exatas");
		Curso curso2 = new Curso("graduação em Contabilidade", "Exatas");
		Curso curso3 = new Curso("Graduacao em Educação Fisica", "Humanas");
		Curso curso4 = new Curso("graduacao em Biologia", "Humanas");
		
		//salvar cursos pelo JPA Repository
		cursoRepository.save(curso1);
		cursoRepository.save(curso2);
		cursoRepository.save(curso3);
		cursoRepository.save(curso4);
		
		//criar alunos nos cursos
		Aluno aluno1 = new Aluno("Jose", curso1);
		Aluno aluno2 = new Aluno("Aline", curso1);
		Aluno aluno3 = new Aluno("Maria", curso4);
		Aluno aluno4 = new Aluno("Pedro", curso4);
		
		//salvar alunos em seus cursos
		alunoRepository.save(aluno1);
		alunoRepository.save(aluno2);
		alunoRepository.save(aluno3);
		alunoRepository.save(aluno4);
		
		//criar grades curriculares dos cursos
		GradeCurricular grade1 = new GradeCurricular("Graduação em Games", aluno1);
		GradeCurricular grade2 = new GradeCurricular("Graduação em Academia de Rua", aluno3);
		
		//salvar grades
		gradeRepository.save(grade1);
		gradeRepository.save(grade2);
		
		//criar materias das grades curriculares
		Set<GradeCurricular> gradesMateria1 = new HashSet<>();
		gradesMateria1.add(grade1);
		Materia materia1 = new Materia("Design", gradesMateria1);
		Materia materia2 = new Materia("Cultura e Moda", gradesMateria1);
		
		//salvar materia
		materiaRepository.save(materia1);
		materiaRepository.save(materia2);
		
		
		//tempo para alterar - para confirmar diferença tabela creationtime e updatetime
		Thread.sleep(3000);
		
		//alterar para confirmar diferença tabela creationtime e updatetime
		curso4.setNome("graduacao em Biomedicina");
		cursoRepository.save(curso4);
		
		//alterar nome do curso e salvar alteração
//		curso2.setNome("Bacharelado em Economia");
//		cursoRepository.save(curso2);
		
		//pesquisar pelo nome completo do curso
//		List<Curso> cursoPorNome = cursoRepository.findCursoByNome("Graduacao em Economia");
//		cursoPorNome.forEach(curso -> System.out.println(curso));
		
		//pesquisar pelo nome do curso que contem 'graduacao'
//		List<Curso> cursoPorNomeContendo = cursoRepository.findCursoByNomeContaining("Graduacao");
//		cursoPorNomeContendo.forEach(curso -> System.out.println(curso));
		
		//pesquisar pelo nome do curso com like
//		List<Curso> cursoPorNomeComo = cursoRepository.findCursoByNomeLike("%Graduacao%");
//		cursoPorNomeComo.forEach(curso -> System.out.println(curso));
		
		//pesquisar com parte do nome independente upper e low case
//		List<Curso> cursoPorNomeLikeIgCase = cursoRepository.findCursoByNomeLikeIgnoreCase("%Gradu%");
//		cursoPorNomeLikeIgCase.forEach(curso -> System.out.println(curso));
		
		//pesquisar usando query simples
//		List<Curso> cursoPorNomeQuery = cursoRepository.findByQueryNome();
//		cursoPorNomeQuery.forEach(curso -> System.out.println(curso));
		
		//pesquisar usando query nativa
//		List<String> cursoPorQueryNativa = cursoRepository.findByQueryNomePorArea();
//		cursoPorQueryNativa.forEach(curso -> System.out.println(curso));
		
		//pesquisar por parametro usando query nativa
//		List<String> cursoPorQueryNativa = cursoRepository.findByQueryNomePorAreaInformada("Exatas");
//		cursoPorQueryNativa.forEach(curso -> System.out.println(curso));
		
		//pesquisar por mais de um parametro usando query nativa
		List<String> cursoPorQueryNativa = cursoRepository.findByQueryNomePorAreaCursoInfo("Exatas", "Graduação em TI");
		cursoPorQueryNativa.forEach(curso -> System.out.println(curso));
		
		//0 retornar curso pelo id
		//1 usar optional para converter em curso, pois repositorio não aceita passar direto para curso
		//2 comando findById é do tipo Optional
//		Optional<Curso> cursoProcurado = cursoRepository.findById(3);
//		Curso cursoFinal = cursoProcurado.orElse(null);
		
		//imprimir curso final
//		System.out.println("O nome do curso procurado é: " + cursoFinal.getNome());
		
		
		//deletar cursos pela entidade e pelo id
//		cursoRepository.delete(curso1);
//		cursoRepository.deleteById(2);
		
		//criando lista de curso e escrevendo no console
//		List<Curso> listaDeCursos = cursoRepository.findAll();
//		listaDeCursos.forEach(curso -> System.out.println(curso));
		
		//escreve o total de cursos
//		System.out.println("Total de Cursos: (" + cursoRepository.count() + ")");
	}

}

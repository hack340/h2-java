package com.curso.database.entities;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PostPersist;
import javax.persistence.PrePersist;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.sun.istack.NotNull;
//import javax.persistence.Table;

@Entity
//@Table(name = "curso_java")
public class Curso {

	//entidade id e generatedvalue fazem com que o h2 gere o id automaticamente
	//e para qualquer BD, pelo generationtype
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name= "nome_do_curso", nullable = false)
	private String nome;
	
	@OneToMany(mappedBy = "curso")
	private List<Aluno> alunos = new ArrayList<>();
	
	@Column(nullable = false)
	private String area;
	
	@CreationTimestamp
	@Column(name = "data_de_criacao")
	private LocalDateTime dataCriacao;
	
	@UpdateTimestamp
	@Column(name = "data_de_atualizacao")
	private LocalDateTime dataAtualizacao;
	
	@NotNull
	private String usuario;
	
	@Transient
	private BigDecimal valorCurso;
	
	@PostPersist
	private void aposPersistirDados() {
		this.nome = this.nome + " POST";
	}
	
	@PrePersist
	private void antesPersistirDados() {
		this.usuario = null;
	}
	
	public LocalDateTime getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(LocalDateTime dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public LocalDateTime getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(LocalDateTime dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public Curso() {
		// TODO Auto-generated constructor stub
	}
	
	public Curso(String nome, String area) {
		super();
		this.nome = nome;
		this.area = area;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getValorCurso() {
		return valorCurso;
	}

	public void setValorCurso(BigDecimal valorCurso) {
		this.valorCurso = valorCurso;
	}

	@Override
	public String toString() {
		return "Curso [id=" + id + ", nome=" + nome + ", area=" + area + "]";
	}

	
	
}
